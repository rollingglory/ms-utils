"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var SchemaRegistryManager_1;
Object.defineProperty(exports, "__esModule", { value: true });
exports.SchemaRegistryManager = void 0;
const confluent_schema_registry_1 = require("@kafkajs/confluent-schema-registry");
const common_1 = require("@nestjs/common");
let SchemaRegistryManager = SchemaRegistryManager_1 = class SchemaRegistryManager {
    constructor({ producerSchemas, consumerSchemas, }) {
        this.ConsumerSchemas = consumerSchemas;
        this.ProducerSchemas = producerSchemas;
        this.logger = new common_1.Logger(SchemaRegistryManager_1.name);
        const { SCHEMA_REGISTRY_HOST, SCHEMA_REGISTRY_USERNAME, SCHEMA_REGISTRY_PASSWORD, } = process.env || {};
        this.schemaRegistry = new confluent_schema_registry_1.SchemaRegistry({
            host: SCHEMA_REGISTRY_HOST,
            auth: {
                username: SCHEMA_REGISTRY_USERNAME,
                password: SCHEMA_REGISTRY_PASSWORD,
            },
        });
    }
    async onModuleInit() {
        await Promise.all([
            this.registerProducerSchema(),
        ]);
    }
    async registerProducerSchema() {
        await Promise.all(this.ProducerSchemas.map(async (schema) => {
            await this.registerSchema(schema);
        }));
    }
    async registerConsumerSchema() {
        await Promise.all(this.ConsumerSchemas.map(async (schema) => {
            await this.registerSchema(schema);
        }));
    }
    async registerSchema(schema) {
        const { name, namespace } = schema || {};
        if (!schema.topic && (!name || !namespace)) {
            return;
        }
        const topic = schema.topic || `${namespace}.${name}`;
        const subject = schema.subject || `${topic}-value`;
        const { id } = await this.schemaRegistry
            .register(schema.value || schema, { subject })
            .catch((e) => {
            this.logger.error(`Error Schema Register of ${topic} : \n${e}`);
            return { id: 0 };
        });
        this.logger.log(`Register schema of ${topic} with ID ${id}`);
    }
};
SchemaRegistryManager = SchemaRegistryManager_1 = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [Object])
], SchemaRegistryManager);
exports.SchemaRegistryManager = SchemaRegistryManager;
//# sourceMappingURL=schema-registry.js.map