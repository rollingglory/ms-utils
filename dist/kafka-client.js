"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var ClientKafkaProvider_1;
Object.defineProperty(exports, "__esModule", { value: true });
exports.ClientKafkaProvider = void 0;
const common_1 = require("@nestjs/common");
const microservices_1 = require("@nestjs/microservices");
const rxjs_1 = require("rxjs");
const lodash_1 = require("lodash");
let ClientKafkaProvider = ClientKafkaProvider_1 = class ClientKafkaProvider extends microservices_1.ClientKafka {
    constructor(options) {
        super(options);
        this.producerPath = options.producerPath;
        this.schemaRegistry = options.schemaRegistry;
        this.plogger = new common_1.Logger(ClientKafkaProvider_1.name);
    }
    send(pattern, data) {
        return (0, rxjs_1.from)(this.sendAsync(pattern, data));
    }
    async sendAsync(pattern, data) {
        return (0, rxjs_1.lastValueFrom)(super.send(pattern, data)).then((res) => {
            this.plogger.debug({ send: res });
            return res;
        });
    }
    emit(pattern, data) {
        return (0, rxjs_1.from)(this.emitAsync(pattern, data));
    }
    async emitAsync(pattern, data) {
        const message = (0, lodash_1.cloneDeep)(data);
        await this.encode(pattern, data);
        return (0, rxjs_1.lastValueFrom)(super.emit(pattern, message)).then((res) => {
            this.plogger.debug({ emit: res });
            return res;
        });
    }
    async encode(topic, payload) {
        var _a, _b;
        const id = ((_a = this.schemaRegistry) === null || _a === void 0 ? void 0 : _a.cache.getLatestRegistryId(`${topic}-value`)) ||
            ((_b = this.schemaRegistry) === null || _b === void 0 ? void 0 : _b.cache.getLatestRegistryId(`${topic}`));
        if (!payload.value) {
            payload = { value: payload };
        }
        if (!id)
            return payload;
        payload.value = await this.schemaRegistry
            .encode(+id, payload.value)
            .catch((e) => {
            this.logger.debug(`${topic} failed to encoded`, e, e === null || e === void 0 ? void 0 : e.paths);
            throw Error('Message Broker: Failed to encode message');
        });
        return payload;
    }
    async decode(message) {
        var _a;
        if (Buffer.isBuffer(message)) {
            return this.schemaRegistry.decode(message);
        }
        if (typeof message === 'object' && message.value) {
            const messageObject = Buffer.isBuffer(message.value)
                ? await ((_a = this.schemaRegistry) === null || _a === void 0 ? void 0 : _a.decode(message.value))
                : message.value;
            this.plogger.debug(`consume: ${message.topic}, offset: ${message.offset}, partition: ${message.partition}`);
            return messageObject;
        }
        return message;
    }
};
ClientKafkaProvider = ClientKafkaProvider_1 = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [Object])
], ClientKafkaProvider);
exports.ClientKafkaProvider = ClientKafkaProvider;
//# sourceMappingURL=kafka-client.js.map