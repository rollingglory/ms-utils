import { SchemaRegistry } from '@kafkajs/confluent-schema-registry';
import { OnModuleInit } from '@nestjs/common';
export declare class SchemaRegistryManager implements OnModuleInit {
    private readonly logger;
    private ConsumerSchemas;
    private ProducerSchemas;
    schemaRegistry: SchemaRegistry;
    constructor({ producerSchemas, consumerSchemas, }: {
        producerSchemas: any[];
        consumerSchemas: any[];
    });
    onModuleInit(): Promise<void>;
    private registerProducerSchema;
    private registerConsumerSchema;
    private registerSchema;
}
