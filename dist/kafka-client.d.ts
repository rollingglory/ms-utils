/// <reference types="node" />
import { SchemaRegistry } from '@kafkajs/confluent-schema-registry';
import { ClientKafka, KafkaOptions } from '@nestjs/microservices';
import { Observable } from 'rxjs';
export declare class ClientKafkaProvider extends ClientKafka {
    private readonly plogger;
    private readonly producerPath;
    schemaRegistry: SchemaRegistry;
    constructor(options: KafkaOptions['options'] & {
        producerPath: string;
        schemaRegistry: SchemaRegistry;
    });
    send<TResult = any, TInput = any>(pattern: any, data: TInput): Observable<TResult>;
    private sendAsync;
    emit<TResult = any, TInput = any>(pattern: any, data: TInput): Observable<TResult>;
    private emitAsync;
    private encode;
    decode(message: Buffer | string | Record<string, unknown>): Promise<any>;
}
