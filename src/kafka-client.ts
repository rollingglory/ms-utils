import { SchemaRegistry } from '@kafkajs/confluent-schema-registry';
import { Injectable, Logger } from '@nestjs/common';
import { ClientKafka, KafkaOptions } from '@nestjs/microservices';
import { from, lastValueFrom, Observable } from 'rxjs';
import { cloneDeep } from 'lodash';

@Injectable()
export class ClientKafkaProvider extends ClientKafka {
  private readonly plogger: Logger;

  private readonly producerPath: string;

  schemaRegistry: SchemaRegistry;

  constructor(
    options: KafkaOptions['options'] & {
      producerPath: string;
      schemaRegistry: SchemaRegistry;
    },
  ) {
    super(options);
    this.producerPath = options.producerPath;
    this.schemaRegistry = options.schemaRegistry;
    this.plogger = new Logger(ClientKafkaProvider.name);
  }

  override send<TResult = any, TInput = any>(
    pattern: any,
    data: TInput,
  ): Observable<TResult> {
    return from(this.sendAsync(pattern, data));
  }

  private async sendAsync(pattern: any, data: any) {
    return lastValueFrom(super.send(pattern, data)).then((res) => {
      this.plogger.debug({ send: res });
      return res;
    });
  }

  override emit<TResult = any, TInput = any>(
    pattern: any,
    data: TInput,
  ): Observable<TResult> {
    return from(this.emitAsync(pattern, data));
  }

  private async emitAsync(pattern: any, data: any) {
    const message = cloneDeep(data);
    await this.encode(pattern, data);

    return lastValueFrom(super.emit(pattern, message)).then((res) => {
      this.plogger.debug({ emit: res });
      return res;
    });
  }

  private async encode(topic: string, payload: any) {
    const id =
      this.schemaRegistry?.cache.getLatestRegistryId(`${topic}-value`) ||
      this.schemaRegistry?.cache.getLatestRegistryId(`${topic}`);

    if (!payload.value) {
      payload = { value: payload };
    }

    if (!id) return payload;

    payload.value = await this.schemaRegistry
      .encode(+id, payload.value)
      .catch((e) => {
        this.logger.debug(`${topic} failed to encoded`, e, e?.paths);
        throw Error('Message Broker: Failed to encode message');
      });

    return payload;
  }

  async decode(
    message: Buffer | string | Record<string, unknown>,
  ): Promise<any> {
    if (Buffer.isBuffer(message)) {
      return this.schemaRegistry.decode(message);
    }

    if (typeof message === 'object' && message.value) {
      const messageObject = Buffer.isBuffer(message.value)
        ? await this.schemaRegistry?.decode(message.value)
        : message.value;
      this.plogger.debug(
        `consume: ${message.topic}, offset: ${message.offset}, partition: ${message.partition}`,
      );

      return messageObject;
    }
    return message;
  }
}
